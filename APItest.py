import http.client
import json

connection = http.client.HTTPConnection('api.football-data.org')
headers = { 'X-Auth-Token': 'baf9a6c1b5344906b70b585cfca9b22d', 'X-Response-Control': 'minified' }

while 1>0:
    print("Enter request URL in the form '/v1/....'")
    add = input()
    connection.request('GET', add, None, headers )
    response = json.loads(connection.getresponse().read().decode())
    file = open("a.json", "w")
    file.write(json.dumps(response))
    file.close()
    print("Response saved to a.json")
    
